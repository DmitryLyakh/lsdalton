# Wrapper of QcMatrix library
add_library(ls_qcmatrix_wrapper
            ls_qcmatrix_wrapper.F90)

# Sets the external QcMatrix library, and a library libls_qcmatrix.a will be generated
set(ExternalProjectCMakeArgs
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/external
    -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
    -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
    -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
    -DCMAKE_Fortran_FLAGS=${CMAKE_Fortran_FLAGS}
    -DCMAKE_Fortran_MODULE_DIRECTORY=${CMAKE_Fortran_MODULE_DIRECTORY}
    -DQCMATRIX_64BIT_INTEGER=${ENABLE_64BIT_INTEGERS}
    -DQCMATRIX_BLAS_64BIT=${ENABLE_64BIT_INTEGERS}
    -DQCMATRIX_ZERO_BASED=OFF
    -DQCMATRIX_ROW_MAJOR=OFF
    -DQCMATRIX_SINGLE_PRECISION=OFF
    -DQCMATRIX_STORAGE_MODE=OFF
    -DQCMATRIX_ENABLE_VIEW=ON
    -DQCMATRIX_ENABLE_HDF5=OFF
    -DQCMATRIX_3M_METHOD=ON
    -DQCMATRIX_STRASSEN_METHOD=ON
    -DQCMATRIX_AUTO_ERROR_EXIT=ON
    -DQCMATRIX_TEST_EXECUTABLE=OFF
    -DQCMATRIX_TEST_3M_METHOD=OFF
    -DLIB_QCMATRIX_NAME=qcmatrix
    -DQCMATRIX_BUILD_ADAPTER=ON
    -DEXTERNAL_BLOCK_MATRIX=OFF
    -DEXTERNAL_COMPLEX_MATRIX=OFF
    -DQCMATRIX_ADAPTER_TYPE=F03
    -DQCMATRIX_EXTERNAL_PATH=${CMAKE_Fortran_MODULE_DIRECTORY}
    -DQCMATRIX_EXTERNAL_LIBRARIES=None
    -DLANG_F_MODULE=qcmatrix_backend
    -DLANG_F_MATRIX=Matrixp
    -DQCMATRIX_Fortran_API=F03
    -DPARENT_MODULE_DIR=${CMAKE_Fortran_MODULE_DIRECTORY}
   )
add_external(qcmatrix)

unset(ExternalProjectCMakeArgs)

# LSDALTON/qcmatrix/qcmatrix_backend.F90 needs the header files of the QcMatrix library
include_directories(${PROJECT_SOURCE_DIR}/external/qcmatrix/include)

# Because QcMatrix will use the matrix module of LSDALTON, we can not put it after
# matrixulib as in the external libraries. Instead, we put QcMatrix library into
# LS_QCMATRIX_LIBS, which will be used for linking together with Gen1Int, OpenRSP
# and TDRSP libraries
#
# Adds libmatrixulib.a to external QcMatrix library due to dependency
# problem (QcMatrix depends on libmatrixulib.a)
set(LS_QCMATRIX_LIBS
    ${PROJECT_BINARY_DIR}/external/lib/libqcmatrix.a
    ${PROJECT_BINARY_DIR}/lib/libmatrixulib.a)
set(LS_QCMATRIX_LIBS
    ${LS_QCMATRIX_LIBS}
    PARENT_SCOPE)

add_dependencies(qcmatrix matrixulib)
add_dependencies(ls_qcmatrix_wrapper matrixulib)
add_dependencies(ls_qcmatrix_wrapper qcmatrix)
