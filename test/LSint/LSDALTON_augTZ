#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_augTZ.info <<'%EOF%'
   LSDALTON_augTZ
   -------------
   Molecule:         NCH
   Wave Function:    HF
   Test Purpose:     check aug-cc-pVTZ basis
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_augTZ.mol <<'%EOF%'
ATOMBASIS
1 HCN molecules placed 20 atomic units apart
STRUCTURE IS NOT OPTIMIZED. 
Atomtypes=3 Nosymmetry
Charge=1. Atoms=1 Basis=3-21G
H   0.0000   0.0000  -1.0000
Charge=7. Atoms=1 Basis=aug-cc-pVTZ
N   0.0000   0.0000   1.5000
Charge=6. Atoms=1 Basis=aug-cc-pVTZ
C   0.0000   0.0000   0.0000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_augTZ.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
.DIIS
.START
TRILEVEL
.CONVTHR
5.0D-3
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_augTZ.check
cat >> LSDALTON_augTZ.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final HF energy:    * \-91.5419657" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="test not correct -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
