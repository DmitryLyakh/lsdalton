#SPECIAL TENSORS OPTIONS
option(TENSORS_ENABLE_REQUEST_MPI3      "Enable request based one-sided MPI3 routines" OFF)
option(TENSORS_ENABLE_BRUTE_FORCE_FLUSH "Brute force flushing of MPI windows"          OFF)

if(TENSORS_ENABLE_REQUEST_MPI3 AND TENSORS_ENABLE_BRUTE_FORCE_FLUSH)
   message(FATAL_ERROR "-- Only one of the options TENSORS_ENABLE_REQUEST_MPI3, TENSORS_ENABLE_BRUTE_FORCE_FLUSH allowed ")
endif()

if(TENSORS_ENABLE_REQUEST_MPI3)
   add_definitions(-DUSE_REQUEST_MPI3_ROUTINES)
endif()

if(TENSORS_ENABLE_BRUTE_FORCE_FLUSH)
   add_definitions(-DVAR_BRUTE_FLUSH)
endif()


if(ENABLE_TENSORS)
    add_definitions(-DVAR_ENABLE_TENSORS)

    include(ExternalProject)
    set(_ScaTeLib_args
        -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
        -DCMAKE_Fortran_COMPILER_ID=${CMAKE_Fortran_COMPILER_ID}
        -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
        -DCMAKE_C_COMPILER_ID=${CMAKE_C_COMPILER_ID}
        -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
        -DCMAKE_CXX_COMPILER_ID=${CMAKE_CXX_COMPILER_ID}
        -DCMAKE_Fortran_FLAGS=${CMAKE_Fortran_FLAGS}
        -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/external
        -DENABLE_MPI=${ENABLE_MPI}
#        -DENABLE_OPENMP=${ENABLE_OMP}
        -DENABLE_GPU=${ENABLE_GPU}
        -DENABLE_64BIT_INTEGERS=${ENABLE_64BIT_INTEGERS}
        -DENABLE_REAL_SP=${ENABLE_REAL_SP}
        -DENABLE_CRAY_WRAPPERS=${ENABLE_CRAY_WRAPPERS}
        -DUSE_MPIF_H=${USE_MPIF_H}
        -DENABLE_TENSOR_CLASS=ON
        -DENABLE_REQUEST_BASED_MPI3=${TENSORS_ENABLE_REQUEST_MPI3}
        -DENABLE_BRUTE_FORCE_FLUSH=${TENSORS_ENABLE_BRUTE_FORCE_FLUSH}
        -DBUILT_AS_SUBMODULE=ON
        )

     ExternalProject_Add(ScaTeLib
        SOURCE_DIR ${PROJECT_SOURCE_DIR}/external/ScaTeLib
        BINARY_DIR ${PROJECT_BINARY_DIR}/external/ScaTeLib-build
        STAMP_DIR ${PROJECT_BINARY_DIR}/external/ScaTeLib-stamp
        TMP_DIR ${PROJECT_BINARY_DIR}/external/ScaTeLib-tmp
        INSTALL_DIR ${PROJECT_BINARY_DIR}/external
        CMAKE_ARGS ${_ScaTeLib_args}
        )

    
    if(CMAKE_Fortran_COMPILER_ID MATCHES Fujitsu) 
       include_directories(${PROJECT_BINARY_DIR}/external/ScaTeLib-build/src)
    else()
       include_directories(${PROJECT_BINARY_DIR}/external/ScaTeLib-build/modules)
    endif()

    unset(_ScaTeLib_args)

    set(LSDALTON_EXTERNAL_LIBS
       ${PROJECT_BINARY_DIR}/external/lib/libScaTeLib.a
        ${LSDALTON_EXTERNAL_LIBS}
        )

    add_dependencies(lsutillib_common1 ScaTeLib)
endif()
