#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION                                                   #
#######################################################################
cat > LSDALTON_donotsavegab.info <<'%EOF%'
   LSDALTON_donotsavegab
   ------------------------
   Molecule:         5 HCN molecules placed 20 atomic units apart
   Wave Function:    CAMB3LYP/STO-2G  alpha=0.21 beta=0.79 mu=0.45
   Test Purpose:     test of .DO NOT SAVE GAB    keyword
%EOF%

#######################################################################
#  MOLECULE INPUT                                                     #
#######################################################################
cat > LSDALTON_donotsavegab.mol <<'%EOF%'
BASIS
STO-2G
5 HCN molecules placed 20 atomic units apart
STO-2G basis 
Atomtypes=3 Nosymmetry Angstrom
Charge=1. Atoms=5
H   0.0   0.0    -1.06992 
H   0.0  20.0    -1.06992 
H   0.0  40.0    -1.06992 
H   0.0  60.0    -1.06992 
H   0.0  80.0    -1.06992 
Charge=7. Atoms=5
N   0.0   0.0     1.15301
N   0.0  20.0     1.15301
N   0.0  40.0     1.15301
N   0.0  60.0     1.15301
N   0.0  80.0     1.15301
Charge=6. Atoms=5
C   0.0   0.0     0.00000
C   0.0  20.0     0.00000
C   0.0  40.0     0.00000
C   0.0  60.0     0.00000
C   0.0  80.0     0.00000
%EOF%

#######################################################################
#  DALTON INPUT                                                       #
#######################################################################
cat > LSDALTON_donotsavegab.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRALS
.DO NOT SAVE GAB
.DEBUGSCREEN
**WAVE FUNCTIONS
.DFT
 CAMB3LYP alpha=0.21 beta=0.79 mu=0.45
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.RH
.DIIS
.CHOLESKY
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT                                                       #
#######################################################################
echo $CHECK_SHELL > LSDALTON_donotsavegab.check
cat >> LSDALTON_donotsavegab.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# FINAL ENERGY test
CRIT1=`$GREP "Final DFT energy\: * \-446\.5590325" $log | wc -l`
TEST[1]=`expr	$CRIT1`
CTRL[1]=1
ERROR[1]="FINAL DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

CRIT1=`$GREP "di_screen_test SUCCESSFUL" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="screening not correct"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
