#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00042atoms_0324basfunc_2240auxbasfunc_nongc_pariJ.info <<'%EOF%'
   00042atoms_0324basfunc_2240auxbasfunc_nongc_pariJ
   -------------
   Molecule:         C20H22 alkane molecules
   Wave Function:    B3LYP/6-31G* Aux=cc-pVTZdenfit
   Profile:          Pari J
   CPU Time:         1 min
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00042atoms_0324basfunc_2240auxbasfunc_nongc_pariJ.mol <<'%EOF%'
BASIS
6-31G* Aux=cc-pVTZdenfit
C20H22
generated from B3LYP/ccpVDZ C6H8 fragments
AtomTypes=2 Generators=0
Charge=6.0 Atoms=20 
C1    -38.6264     0.000000   0.738029
C2    -36.3995     0.000000   -0.4905
C3    -33.9471     0.000000   0.738029
C4    -31.7098     0.000000   -0.507001
C5    -29.2573     0.000000   0.721527
C6    -27.02       0.000000   -0.523503
C7    -24.5676     0.000000   0.705025
C8    -22.3302     0.000000   -0.540005
C9    -19.8778     0.000000   0.688523
C10   -17.6404     0.000000   -0.556507
C11   -15.188      0.000000   0.672021
C12   -12.9507     0.000000   -0.573009
C13   -10.4982     0.000000   0.655519
C14   -8.26088     0.000000   -0.589511
C15   -5.80845     0.000000   0.639017
C16   -3.5711      0.000000   -0.606013
C17   -1.11868     0.000000   0.622515
C18   1.11868      0.000000   -0.622515
C19   3.5711       0.000000   0.606013
C20   5.79792      0.000000   -0.622515
Charge=1.0 Atoms=22 
H1    -40.4209     0.000000   -0.281083
H2    -38.7141     0.000000   2.80444
H3    -36.3896     0.000000   -2.56356
H4    -33.9406     0.000000   2.81194
H5    -31.7163     0.000000   -2.58092
H6    -29.2508     0.000000   2.79544
H7    -27.0266     0.000000   -2.59742
H8    -24.561      0.000000   2.77894
H9    -22.3368     0.000000   -2.61392
H10   -19.8712     0.000000   2.76244
H11   -17.647      0.000000   -2.63042
H12   -15.1814     0.000000   2.74594
H13   -12.9572     0.000000   -2.64692
H14   -10.4917     0.000000   2.72943
H15   -8.26744     0.000000   -2.66343
H16   -5.80189     0.000000   2.71293
H17   -3.57767     0.000000   -2.67993
H18   -1.11211     0.000000   2.69643
H19   1.11211      0.000000   -2.69643
H20   3.56119      0.000000   2.67907
H21   5.88561      0.000000   -2.68892
H22   7.5925       0.000000   0.396597
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00042atoms_0324basfunc_2240auxbasfunc_nongc_pariJ.dal <<'%EOF%'
**PROFILE
.COULOMB
**INTEGRALS
.PARI-J
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.START
.H1DIAG
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00042atoms_0324basfunc_2240auxbasfunc_nongc_pariJ.check
cat >> 00042atoms_0324basfunc_2240auxbasfunc_nongc_pariJ.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Coulomb energy, mat_dotproduct\(D,J\)\= * 1969\.95126043" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
