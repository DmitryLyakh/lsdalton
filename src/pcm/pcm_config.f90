!> @file
!> configure PCM input LSDALTON-side
module pcm_config

use, intrinsic :: iso_c_binding
use typedeftype, only: lssetting
use pcm_precision
use pcmsolver
use lstiming
use pcm_utils, only: collect_nctot, collect_atoms, collect_symmetry_info
use ls_util, only: lsheader
use pcm_write, only: init_host_writer, host_writer

implicit none

private

! if false the interface will refuse to be accessed
logical :: is_initialized = .false.

public pcmtype
public PCM
public pcm_configure
public pcm_initialize
public pcm_finalize
public pcm_input

type pcmtype
     logical :: do_pcm
     logical :: separate
     integer(kind=regint_k) :: print_level
     logical :: host_provides_input
end type pcmtype

type PCM
     integer :: global_print_unit
     integer :: global_error_unit
     type(lssetting) :: integral_settings
     integer(4)  :: nr_points
     integer(4)  :: nr_points_irr
     real(c_double), allocatable :: tess_cent(:)
end type PCM

type(pcmtype), public    :: pcm_cfg
type(PCM),     public    :: pcm_global
type(c_ptr),   public    :: context_
integer                  :: global_print_unit = -1

! *PCMSOL section
! cavity specification *PCMSOL section
character(len=8), public, save :: pcmmod_cavity_type = 'gepol  '//c_null_char
integer(kind=regint_k), public, save  :: pcmmod_patch_level = 2
real(kind=dp), public, save  :: pcmmod_coarsity = 0.5
real(kind=dp), public, save  :: pcmmod_cavity_area = 0.3
real(kind=dp), public, save  :: pcmmod_min_distance = 0.1
integer(kind=regint_k), public, save  :: pcmmod_der_order = 4
logical, public, save :: pcmmod_scaling = .true.
character(len=8), public, save :: pcmmod_radii_set = 'bondi  '//c_null_char
character(len=20), public, save :: pcmmod_restart_name = 'cavity.npz         '//c_null_char
real(kind=dp), public, save  :: pcmmod_min_radius = 100.0
! solver specification *PCMSOL section
character(len=7), public, save :: pcmmod_solver_type = 'iefpcm'//c_null_char
character(len=16), public, save :: pcmmod_solvent = '               '//c_null_char
character(len=11), public, save :: pcmmod_equation_type = 'secondkind'//c_null_char
real(kind=dp), public, save  :: pcmmod_correction = 0.0
real(kind=dp), public, save  :: pcmmod_probe_radius = 1.0
! green specification *PCMSOL section
character(len=7), public, save :: pcmmod_inside_type = 'vacuum'//c_null_char
character(len=22), public, save :: pcmmod_outside_type = 'uniformdielectric    '//c_null_char
real(kind=dp), public, save :: pcmmod_outside_epsilon = 1.0

contains

!> \brief initializes PCM configuration
!> \author R. Di Remigio
!> \date 2015
!> \param pcm_input PCM input section
subroutine pcm_configure(pcm_input)

  type(pcmtype), intent(inout) :: pcm_input

  ! Polarizable continuum model calculation is turned off by default
  pcm_input%do_pcm = .false.
  ! Use of separate charges and potentials is turned off by default
  pcm_input%separate = .false.
  ! Print level is set to 0 by default
  pcm_input%print_level = 0
  pcm_cfg = pcm_input

end subroutine pcm_configure

!> \brief initializes interface to PCM
!> \author R. Di Remigio
!> \date 2015
!> \param setting integral settings
!> \param print_unit global print unit
!> \param err_unit global error unit
subroutine pcm_initialize(setting, print_unit, err_unit)

  type(lssetting), intent(in)  :: setting
  integer :: print_unit, err_unit

  type(PCM) :: pcm_tmp
  real(kind=dp) :: tstart, tend
  integer(c_int) :: nr_nuclei
  real(c_double), allocatable :: charges(:)
  real(c_double), allocatable :: centers(:)
  integer(c_int) :: symmetry_info(4)
  type(PCMInput) :: host_input

  if (.not. pcmsolver_is_compatible_library()) then
      call lsquit('Error: incompatible version of PCMSolver library.', -1)
  end if

  nr_nuclei = collect_nctot()
  allocate(charges(nr_nuclei))
  charges = 0.0_c_double
  allocate(centers(3*nr_nuclei))
  centers = 0.0_c_double
  call collect_atoms(charges, centers)
  call collect_symmetry_info(symmetry_info)
  if (pcm_cfg%host_provides_input) then
      host_input = pcmsolver_input()
      context_ = pcmsolver_new(PCMSOLVER_READER_HOST, nr_nuclei, charges, centers, symmetry_info, host_input, c_funloc(host_writer))
  else
      context_ = pcmsolver_new(PCMSOLVER_READER_OWN, nr_nuclei, charges, centers, symmetry_info, host_input, c_funloc(host_writer))
  end if

  deallocate(charges)
  deallocate(centers)

  pcm_tmp%nr_points = pcmsolver_get_cavity_size(context_)
  pcm_tmp%nr_points_irr = pcmsolver_get_irreducible_cavity_size(context_)

  ! Time initialization of tess_cent
  call lstimer('START ', tstart, tend, print_unit)
  allocate(pcm_tmp%tess_cent(3*pcm_tmp%nr_points))
  pcm_tmp%tess_cent = 0.0_dp
  call pcmsolver_get_centers(context_, pcm_tmp%tess_cent)
  call lstimer('PCMpts', tstart, tend, print_unit)

  pcm_tmp%integral_settings = setting
  pcm_tmp%global_print_unit = print_unit
  pcm_tmp%global_error_unit = err_unit

  pcm_global = pcm_tmp

  is_initialized = .true.

  call pcmsolver_print(context_)

end subroutine pcm_initialize

!> \brief finalizes interface to PCM-SCF
!> \author R. Di Remigio
!> \date 2015
!>
!> This subroutine finalizes various global objects
subroutine pcm_finalize()

   if (.not. is_initialized) then
       call lsquit('Error: PCM was never initialized.', -1)
   else
       call pcmsolver_save_surface_functions(context_)
       ! Free the memory taken from the free store both in Fortran and in C++
       deallocate(pcm_global%tess_cent)
       call pcmsolver_delete(context_)
   end if

   is_initialized = .false.

end subroutine pcm_finalize

!> \brief processes PCM input section to its default values
!> \author R. Di Remigio
!> \date 2014
!> \param pcm_inp PCM input section
!> \param readword
!> \param keyword
!> \param lucmd
!> \param lupri
subroutine pcm_input(pcm_inp, readword, keyword, lucmd, lupri)

   type(pcmtype)           :: pcm_inp
   integer                 :: lucmd, lupri
   integer                 :: NError, NWarning
   integer, dimension(1:2) :: KWordInd
   character(len = 1)      :: Prompt
   character(len = 70)     :: Keyword
   character(len = 7), dimension(1:2) :: kwordtable = &
     (/ '.SEPARA', '.PRINT ' /)
   logical :: file_exist, const_ts
   logical, intent(inout)   :: readword
   integer                  :: filestatus

   pcm_inp%host_provides_input = .false.
   call lsheader(lupri, &
   'Input processing for the Polarizable Continuum Model module')
   nerror          = 0
   nwarning        = 0
   ReadKeywords: do
      Read(lucmd,'(A70)', IOStat = FileStatus) Keyword
      !      WRITE(*,*)'READ KEYWORDS',Keyword(1:7)
      If (FileStatus > 0) Call LSQuit('Error reading lucmd', lupri)
      If ((FileStatus < 0)) Exit
      Prompt = Keyword(1:1)
      If ((Prompt == '#') .or. (Prompt == '!')) Cycle
      If (Prompt == '*') then
         ReadWord = .FALSE.
         Exit
      Endif
      If (Prompt == '.') Then
        If (Any(KWordTable(1:size(KWordTable)) .EQ. Keyword(1:7))) Then
          Select Case (Keyword(1:7))
            ! Use separate or total potential and charges
            case('.SEPARA')
              pcm_inp%separate = .true.
            ! Print level
            case('.PRINT ')
              read(lucmd,*) pcm_inp%print_level
            case default
              Write(lupri,'(/,3A,/)') ' Keyword "',Keyword, &
                '" is not yet implemented in PCM_Input_Proc.'
          End Select
        Else
          Write(lupri,'(/,3A,/)') ' Keyword "',Keyword, &
            '" not recognized in PCM_Input_Proc.'
          Call LSQuit('Illegal keyword in PCM_Input_Proc.',lupri)
        End If
      Endif
   enddo ReadKeywords
   pcm_cfg = pcm_inp
   call report_after_pcm_input(lupri, pcm_inp)

end subroutine pcm_input

!> \brief print relevant setting of both LSDALTON and PCMSolver
!> \author R. Di Remigio
!> \date 2014
!> \param print_unit the printing unit to be used
!> \param pcm_input PCM input section
subroutine report_after_pcm_input(print_unit, pcm_cfg)
   integer, optional, intent(in) :: print_unit
   type(pcmtype), intent(in)     :: pcm_cfg

   if (present(print_unit)) then
      global_print_unit = print_unit
      ! Initialize host writer
      call init_host_writer(global_print_unit)
      write(global_print_unit, *) &
      ' ===== Polarizable Continuum Model calculation set-up ====='
      write(global_print_unit, *) &
      '* Polarizable Continuum Model using PCMSolver external module:'
      write(global_print_unit, *) &
      '  1: Converged potentials and charges at tesserae representative points written on file.'

      if (pcm_cfg%separate) then
         write(global_print_unit, *) &
         '  2: Separate potentials and apparent charges in nuclear and electronic.'
      else
         write(global_print_unit, *) &
         '  2: Use total potentials and apparent charges.'
      end if

      if (pcm_cfg%print_level > 5 .and. pcm_cfg%print_level < 10) then
         write(global_print_unit, *) &
         '  3: Print potentials at tesserae representative points.'
      else if (pcm_cfg%print_level > 10) then
         write(global_print_unit, *) &
         '  3: Print potentials and charges at tesserae representative points.'
      else
         write(global_print_unit, *) &
         '  3: Do not print potentials and charges.'
      end if
   else
      write(*, *) &
      ' ===== Polarizable Continuum Model calculation set-up ====='
      write(*, *) &
      '* Polarizable Continuum Model using PCMSolver external module:'
      write(*, *) &
      '  1: Converged potentials and charges at tesserae representative points written on file.'

      if (pcm_cfg%separate) then
         write(*, *) &
         '  2: Separate potentials and apparent charges in nuclear and electronic.'
      else
         write(*, *) &
         '  2: Use total potentials and apparent charges.'
      end if

      if (pcm_cfg%print_level > 5 .and. pcm_cfg%print_level < 10) then
         write(*, *) &
         '  3: Print potentials at tesserae representative points.'
      else if (pcm_cfg%print_level > 10) then
         write(*, *) &
         '  3: Print potentials and charges at tesserae representative points.'
      else
         write(*, *) &
         '  3: Do not print potentials and charges.'
      end if
   end if

end subroutine report_after_pcm_input

!> \brief sets PCMSolver input parameters from LSDALTON input
!> \author R. Di Remigio
!> \date 2014
!> \param cavity struct holding cavity parameters
! Performs syntactic checks on PCMSolver input and fills the data structures
! holding input data
function pcmsolver_input() result(host_input)

  type(PCMInput) :: host_input

  character(kind=c_char, len=1) :: cavity_type(8)
  character(kind=c_char, len=1) :: radii_set(8)
  character(kind=c_char, len=1) :: restart_name(20)
  character(kind=c_char, len=1) :: solver_type(7)
  character(kind=c_char, len=1) :: solvent(16)
  character(kind=c_char, len=1) :: equation_type(11)
  character(kind=c_char, len=1) :: inside_type(7)
  character(kind=c_char, len=1) :: outside_type(22)

  host_input%cavity_type  = pcmsolver_fstring_to_carray(pcmmod_cavity_type)
  host_input%patch_level  = int(pcmmod_patch_level, kind=c_int)
  host_input%coarsity     = pcmmod_coarsity
  host_input%area         = pcmmod_cavity_area
  host_input%min_distance = pcmmod_min_distance
  host_input%der_order    = int(pcmmod_der_order, kind=c_int)
  host_input%scaling      = pcmmod_scaling
  host_input%radii_set    = pcmsolver_fstring_to_carray(pcmmod_radii_set)
  host_input%restart_name = pcmsolver_fstring_to_carray(pcmmod_restart_name)
  host_input%min_radius   = pcmmod_min_radius

  host_input%solver_type   = pcmsolver_fstring_to_carray(pcmmod_solver_type)
  host_input%solvent       = pcmsolver_fstring_to_carray(pcmmod_solvent)
  host_input%equation_type = pcmsolver_fstring_to_carray(pcmmod_equation_type)
  host_input%correction    = pcmmod_correction
  host_input%probe_radius  = pcmmod_probe_radius

  host_input%inside_type     = pcmsolver_fstring_to_carray(pcmmod_inside_type)
  host_input%outside_epsilon = pcmmod_outside_epsilon
  host_input%outside_type    = pcmsolver_fstring_to_carray(pcmmod_outside_type)

end function pcmsolver_input

end module pcm_config
