#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > PipekMezeyMulliken.info <<'%EOF%'
   Localize orbitals using Pipek-Mezey 
   --------------------------------------
   Molecule:         water 
   Wave Function:    HF / cc-pVDZ
   Test Purpose:     Test Pipek-Mezey localization function 
                     using Mulliken population analysis (Ida-Marie Hoeyvik)
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > PipekMezeyMulliken.mol <<'%EOF%'
BASIS
cc-pVDZ


Atomtypes=2 Nosymmetry
Charge=8.0 Atoms=1
O    -2.66610950   0.12039950   0.00000000
Charge=1.0 Atoms=2
H    -3.30256970  -0.78553700   1.43043210
H    -3.30256970  -0.78553700  -1.43043210
%EOF%
#######################################################################
#  DALTON INPUT
#######################################################################
cat > PipekMezeyMulliken.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.START
TRILEVEL
.LCM
**LOCALIZE ORBITALS
.PIPEKM(MULL)
.MACRO IT
100
*END OF INPUT  
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >PipekMezeyMulliken.check
cat >>PipekMezeyMulliken.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# HF ENERGY 
CRIT1=`$GREP "Final HF energy\: * \-76\.0267" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# KURTOSIS
CRIT1=`$GREP "Max\. sigma_4 and orb\.number\:    3\." $log | wc -l`
TEST[2]=`expr   $CRIT1`
CTRL[2]=1
ERROR[2]="Kurtosis is not correct"

# Memory test for matrices
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak for matrices -"


PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%




