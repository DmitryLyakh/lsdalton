module lsmpi_typeParam
  use precision
#ifdef VAR_MPI
  use lsmpi_module
#endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!
!integer conversion factor!
!!!!!!!!!!!!!!!!!!!!!!!!!!!
  integer,parameter :: MaxIncreaseSize = 50000000 !0.4 GB

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Checking and measuring variables!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  logical                     :: AddToBuffer
  integer(kind=long)          :: iLog,iDP,iInt4,iInt8,iCha,iSho
  integer(kind=long)          :: nLog,nDP,nInteger4,nInteger8,nCha,nShort
  real(realk),pointer         :: lsmpibufferDP(:)
  integer(kind=4),pointer     :: lsmpibufferInt4(:)
  integer(kind=8),pointer     :: lsmpibufferInt8(:)
  !note lsmpibufferSho is needed because the reduction is different for short integer
  !then for integer. lsmpi_int_reduction is used for lsmpibufferInt4 which uses MPI_SUM
  !lsmpi_sho_reduction is used for lsmpibufferSho which uses MPI_MAX
  integer(kind=short),pointer :: lsmpibufferSho(:) 
  logical,pointer             :: lsmpibufferLog(:)
  character,pointer           :: lsmpibufferCha(:)
  integer,parameter           :: incremLog=169,incremDP=100,incremInteger=626
  integer,parameter           :: incremCha             = 1510
  real(realk)                 :: poketime              = 0.0E0_realk
  integer(kind=long)          :: poketimes             = 0
  real(realk)                 :: time_lsmpi_win_unlock = 0.0E0_realk
  real(realk)                 :: time_lsmpi_wait       = 0.0E0_realk
  real(realk)                 :: time_lsmpi_win_flush  = 0.0E0_realk

  integer(kind=long),save :: incremLogSave,incremDPSave,incremIntegerSave,incremChaSave

!$OMP THREADPRIVATE(AddToBuffer,iLog,iDP,iInt4,iInt8,iCha,iSho,&
!$OMP nLog,nDP,nInteger4,nInteger8,nCha,lsmpibufferDP,lsmpibufferInt4,&
!$OMP lsmpibufferInt8,lsmpibufferLog,lsmpibufferCha,nShort,lsmpibufferSho)

public :: NullifyMPIbuffers,lsmpi_buffer_set_increm,lsmpi_buffer_get_increm,&
     & PrintMPIbuffersizes,GET_MPI_COMM_SELF,AddToBuffer,iLog,iDP,iInt4,iInt8,iCha,iSho,&
     & nLog,nDP,nInteger4,nInteger8,nCha,lsmpibufferDP,lsmpibufferInt4,&
     & lsmpibufferInt8,lsmpibufferLog,lsmpibufferCha,nShort,lsmpibufferSho,&
     & time_lsmpi_win_flush,time_lsmpi_wait,time_lsmpi_win_unlock,poketimes,&
     & poketime,incremCha,incremLog,incremDP,incremInteger,MaxIncreaseSize,&
     & get_rank_for_comm,LSMPI_MYFAIL, lsmpi_comm_free, lsmpi_barrier,&
     & LSMPI_GET_COUNT

private

contains
    !> Get rank number within a specific communicator
  !> \author Kasper Kristensen
  !> \date March 2011
  subroutine get_rank_for_comm(comm,rank)
    implicit none
    !> Communicator 
    integer(kind=ls_mpik),intent(in) :: comm
    !> Rank number in communicator group
    integer(kind=ls_mpik),intent(inout) :: rank
    integer(kind=ls_mpik) :: ierr
    ierr=0
#ifdef VAR_MPI
    call MPI_COMM_RANK(comm,rank,ierr)
#else
    rank = 0
#endif
    if(ierr/=0) then
       call lsquit('get_rank_for_comm: Something wrong!',-1)
    end if
  end subroutine get_rank_for_comm

  subroutine LSMPI_MYFAIL(IERR)
    implicit none
    integer(kind=ls_mpik) :: IERR
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: IERR2,IERRCL
    CHARACTER(len=40) :: ERRBUF
    CALL MPI_ERROR_CLASS(IERR,IERRCL,IERR2)
    IF (IERRCL.EQ.MPI_SUCCESS) THEN
       ERRBUF = 'No error'
    ELSE IF (IERRCL.EQ.MPI_ERR_BUFFER) THEN
       ERRBUF = 'Invalid buffer pointer'
    ELSE IF (IERRCL.EQ.MPI_ERR_COUNT) THEN
       ERRBUF = 'Invalid cnt argument'
    ELSE IF (IERRCL.EQ.MPI_ERR_TYPE) THEN
       ERRBUF = 'Invalid datatype argument'
    ELSE IF (IERRCL.EQ.MPI_ERR_TAG) THEN
       ERRBUF = 'Invalid tag argument'
    ELSE IF (IERRCL.EQ.MPI_ERR_COMM) THEN
       ERRBUF = 'Invalid communicator'
    ELSE IF (IERRCL.EQ.MPI_ERR_RANK) THEN
       ERRBUF = 'Invalid rank'
    ELSE IF (IERRCL.EQ.MPI_ERR_REQUEST) THEN
       ERRBUF = 'Invalid request (handle)'
    ELSE IF (IERRCL.EQ.MPI_ERR_ROOT) THEN
       ERRBUF = 'Invalid root'
    ELSE IF (IERRCL.EQ.MPI_ERR_GROUP) THEN
       ERRBUF = 'Invalid group'
    ELSE IF (IERRCL.EQ.MPI_ERR_OP) THEN
       ERRBUF = 'Invalid operation'
    ELSE IF (IERRCL.EQ.MPI_ERR_TOPOLOGY) THEN
       ERRBUF = 'Invalid topology'
    ELSE IF (IERRCL.EQ.MPI_ERR_DIMS) THEN
       ERRBUF = 'Invalid dimension argument'
    ELSE IF (IERRCL.EQ.MPI_ERR_ARG) THEN
       ERRBUF = 'Invalid argument of some other kind'
    ELSE IF (IERRCL.EQ.MPI_ERR_UNKNOWN) THEN
       ERRBUF = 'Unknown error'
    ELSE IF (IERRCL.EQ.MPI_ERR_TRUNCATE) THEN
       ERRBUF = 'Message truncated on receive'
    ELSE IF (IERRCL.EQ.MPI_ERR_OTHER) THEN
       ERRBUF = 'Known error not in this list'
    ELSE IF (IERRCL.EQ.MPI_ERR_INTERN) THEN
       ERRBUF = 'Internal MPI (implementation) error'
    ELSE IF (IERRCL.EQ.MPI_ERR_IN_STATUS) THEN
       ERRBUF = 'Error code is in lsmpi_status'
    ELSE IF (IERRCL.EQ.MPI_ERR_PENDING) THEN
       ERRBUF = 'Pending request'
    ELSE IF (IERRCL.EQ.MPI_ERR_LASTCODE) THEN
       ERRBUF = 'Last error code'
    ELSE
       !        something we didn't know ...
       WRITE(6,'(A,I4)') 'MPI error class',IERRCL
    END IF
    !
    WRITE(6,'(/A)') ' ERROR in MPI : '//ERRBUF
    !
    CALL LSQUIT('Error detected in MPI. Please consult dalton output!',-1)
    !
#endif
  end subroutine LSMPI_MYFAIL

    subroutine lsmpi_barrier(comm)
#ifdef VAR_MPI
    implicit none
    integer(kind=ls_mpik) :: ierr,comm
    IERR=0
    call MPI_BARRIER(comm,ierr)
#endif 
  end subroutine lsmpi_barrier

    subroutine lsmpi_comm_free(comm)
#ifdef VAR_MPI
    implicit none
    integer(kind=ls_mpik) :: comm
    integer(kind=ls_mpik) :: ierr
    IERR=0
    call MPI_COMM_FREE(comm, IERR)
#endif 
  end subroutine lsmpi_comm_free

  SUBROUTINE NullifyMPIbuffers()
    implicit none
    nullify(lsmpibufferDP)
    nullify(lsmpibufferInt4)
    nullify(lsmpibufferInt8)
    nullify(lsmpibufferLog)
    nullify(lsmpibufferCha)
    nLog=0
    nDP=0
    nInteger4=0
    nInteger8=0
    nCha=0
    iLog=0
    iDP=0
    iInt4=0
    iInt8=0
    iCha=0
    incremLogSave=incremLog
    incremDPSave=incremDP
    incremIntegerSave=incremInteger
  END SUBROUTINE NULLIFYMPIBUFFERS

  SUBROUTINE LSMPI_BUFFER_SET_INCREM(incremLogIn,incremDPIn,incremIntegerIn,incremChaIn)
    implicit none
    integer(kind=long),intent(in) :: incremLogIn,incremDPIn,incremIntegerIn,incremChaIN

    incremLogSave=incremLogIn
    incremDPSave=incremDPIn
    incremIntegerSave=incremIntegerIn
    incremChaSave = incremChaIn

  END SUBROUTINE LSMPI_BUFFER_SET_INCREM

  SUBROUTINE LSMPI_BUFFER_GET_INCREM(incremLogIn,incremDPIn,incremIntegerIn,incremChaIn)
    implicit none
    integer(kind=long),intent(inout) :: incremLogIn,incremDPIn,incremIntegerIn,incremChaIn

    incremLogIn = incremLogSave
    incremDPIn = incremDPSave
    incremIntegerIn = incremIntegerSave
    incremChaIn = incremChaSave

  END SUBROUTINE LSMPI_BUFFER_GET_INCREM

  SUBROUTINE LSMPI_GET_COUNT(lsmpi_status,COUNT2)
    implicit none
    integer(kind=ls_mpik) :: COUNT2
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: lsmpi_status(MPI_STATUS_SIZE)
    integer(kind=ls_mpik) :: IERR

    call MPI_GET_COUNT(lsmpi_status, MPI_DOUBLE_PRECISION, COUNT2,IERR)
    IF(ierr.NE.0)CALL LSMPI_MYFAIL(IERR)
#else
    integer(kind=ls_mpik) :: lsmpi_status(:)
#endif
  END SUBROUTINE LSMPI_GET_COUNT

  SUBROUTINE PrintMPIbuffersizes()
    implicit none
    print*,'# DoublePrecison elements',nDP
    print*,'# Integer 4 elements     ',nInteger4
    print*,'# Integer 8 elements     ',nInteger8
    print*,'# Logical elements       ',nLog
    print*,'# Character elements     ',nCha
  END SUBROUTINE PRINTMPIBUFFERSIZES

#ifdef VAR_MPI
    SUBROUTINE GET_MPI_COMM_SELF(outputcomm)
      implicit none
      integer(kind=ls_mpik),intent(inout) :: OutputComm   
      OutputComm = MPI_COMM_SELF !Predefined Communicator - the local processor
    END SUBROUTINE GET_MPI_COMM_SELF
#endif

  end module lsmpi_typeParam


